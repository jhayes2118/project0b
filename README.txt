questions::.....
TestList.java

//// TODO also try with a LinkedList - does it make any difference?
It works with both. 

list.remove(5); // what does this method do? Removes // removes at index 5

list.remove(Integer.valueOf(5)); // what does this one do?//removes all instances of 5 in the list

TestIterator
list = new LinkedList<Integer>();
		// TODO also try with a LinkedList - does it make any difference?
		Works as well with a Linked List
		
// TODO what happens if you use list.remove(77)?
this does not work because that is calling to remove the list item at index 77.

TestPerformace
size = 10
testLinkedListAddRemove - .112 s
testArrayListAddRemove - .067 s
testLinkedListAccess - .038 s
testArrayListAccess - .019 s

size = 100

testLinkedListAddRemove - .250 s
testArrayListAddRemove - .241 s
testLinkedListAccess - .193 s
testArrayListAccess - .023 s

size 1000

testLinkedListAddRemove - .070 s
testArrayListAddRemove - 1.598 s
testLinkedListAccess - .354 s
testArrayListAccess - .018 s

size 10000
testLinkedListAddRemove - .077 s
testArrayListAddRemove - 15.997 s
testLinkedListAccess - 9.553 s
testArrayListAccess - .018 s

Array lists work faster at accessing the data in the list when the list size grows
LinkLists Adding and removing is much quicker regardless of the size of the data. 






 
		